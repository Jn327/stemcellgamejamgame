﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum cell_type
{
	spawner,
	attacker_type_a,
	attacker_type_b,
	attacker_type_c
};

public enum avaliable_action
{
	Spawn,
	Attack,
};

public class CellCreater : MonoBehaviour
{
	public static GameObject Create_Cell (int player_num, cell_type type, Vector2 cell_position, GameObject cell_object, avaliable_action actions)
	{
		GameObject cell = (GameObject)(Instantiate (cell_object, Vector3.zero, Quaternion.identity));
		cell.AddComponent <CellStats> ();
		CellStats p_c = cell.GetComponent <CellStats> ();
		p_c.Set_position (cell_position);
		p_c.Set_celltype (type);
		p_c.Set_player_number (player_num);
		p_c.Set_action (actions);
		cell.gameObject.tag = "Cell";
		return cell;
	}
}
