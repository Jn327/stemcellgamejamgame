﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class UIController : MonoBehaviour
{
    public GameObject pauseMenu;
    private int _numberOfPlayers = 2;

    public Slider numberOfPlayersSlider;
    public Text numberOfPlayersText;

    void Start ()
    {
        if (numberOfPlayersSlider)
        {
            _numberOfPlayers = PlayerPrefs.GetInt("numberOfPlayers");
            numberOfPlayersSlider.value = _numberOfPlayers;

            if (numberOfPlayersText)
                numberOfPlayersText.text = "Number of Players: " + _numberOfPlayers;
        }
    }

    public void LoadLevel(int i)
    {
        PlayerPrefs.SetInt("numberOfPlayers", _numberOfPlayers);
        Time.timeScale = 1; //Just incase...
        Application.LoadLevel(i);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void TogglePause()
    {
        bool isPaused = (Time.timeScale != 0) ? true : false;
        Time.timeScale = (isPaused) ? 0 : 1;

        if (!pauseMenu) return;
        pauseMenu.SetActive(isPaused);
    }

    public void setNumberOfPlayers()
    {
        if (!numberOfPlayersSlider) return;

        int i = (int)numberOfPlayersSlider.value;


        if (_numberOfPlayers == i) return;

        _numberOfPlayers = i;

        if (numberOfPlayersText)
            numberOfPlayersText.text = "Number of Players: "+_numberOfPlayers;
    }
}
