﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CellMenuController : MonoBehaviour
{
    private RectTransform _rect;
    private Animator _animator;

    private CellStats _currentCell;


	// Use this for initialization
	void Start ()
    {
        _rect = this.GetComponent<RectTransform>();
        _animator = this.GetComponent<Animator>();
	}

    public CellStats getcurrentCell()
    {
        return this._currentCell;
    }

    public void setcurrentCell(CellStats value)
    {
        this._currentCell = value;

        Vector3 pos = Camera.main.WorldToScreenPoint(_currentCell.Get_position());
        setAnchoredPos(new Vector2(pos.x, pos.y));

        _animator.SetTrigger("Open");
    }

    public void setAnchoredPos(Vector2 value)
    {
        if (!_rect)
            _rect = this.GetComponent<RectTransform>();

        if (!_rect) return;

        _rect.anchoredPosition = new Vector2(value.x, value.y);
    }


    public void closeMenu()
    {
       _animator.SetTrigger("Close");
    }
}
