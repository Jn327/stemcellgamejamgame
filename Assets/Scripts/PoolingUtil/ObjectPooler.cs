﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPooler : MonoBehaviour {

	public ObjectPool[] pools;
	public static ObjectPooler OP;

	// Use this for initialization
	void Start () {

		if (OP != null) {
			GameObject.Destroy(OP.gameObject);
		}
		OP = this;

		for (int p = 0; p < pools.Length; p++) 
		{
			if (!pools[p].ParentOfPrefab)
			{
				GameObject poolsParent = new GameObject();
				pools[p].ParentOfPrefab = poolsParent;
				poolsParent.transform.parent = transform;
				poolsParent.name = pools[p].name;
			}

			for (int i = 0; i < pools[p].pooledAmount; i++) {
				createObject (p, false); 
			}
		}
	}


	public GameObject createObject (int poolIndex, bool setActive)
	{
		GameObject obj = (GameObject)Instantiate(pools[poolIndex].prefab);
		obj.transform.parent = pools[poolIndex].ParentOfPrefab.transform;

		PooledObject poolref = obj.GetComponent<PooledObject> ();
		if (poolref == null) {
			obj.AddComponent<PooledObject>();
			poolref = obj.GetComponent<PooledObject> ();
		}
		poolref.SetPoolIndex(poolIndex, pools[poolIndex].returnToPoolTimer);

		pools[poolIndex].poolStack.Push(obj);
		obj.SetActive (setActive);

		pools[poolIndex].AmountInstantiated ++;
		return obj;
	}


	//Adds an object to the pool at given poolindex
	public void AddToPool(GameObject obj, int poolIndex)
	{
		pools[poolIndex].poolStack.Push(obj);
		obj.SetActive (false);

		if (pools[poolIndex].willReturnEldestIfNoneAvailable)
		{
			if (pools[poolIndex].spawnedFromPoolButNotInPool.Contains(obj))
			{
				pools[poolIndex].spawnedFromPoolButNotInPool.Remove(obj);
			}
		}
	}
	
	
	//gets an object from the pool and removes it from it's stack, sets it's active to activate.
	public GameObject GetPooledObject(int poolIndex)
	{
		if (pools[poolIndex].poolStack.Count <= 0)
		{
			if (pools[poolIndex].willReturnEldestIfNoneAvailable)
			{
				while (pools[poolIndex].spawnedFromPoolButNotInPool[0] == null) //incase the object has been deleted and not removed first.
				{
					pools[poolIndex].spawnedFromPoolButNotInPool.Remove(pools[poolIndex].spawnedFromPoolButNotInPool[0]);
					if (pools[poolIndex].spawnedFromPoolButNotInPool.Count <= 0)
					{
						return null;
					}
				}
				
				GameObject obj = pools[poolIndex].spawnedFromPoolButNotInPool[0];
				obj.SetActive(false);
				obj.SetActive(true);
				pools[poolIndex].spawnedFromPoolButNotInPool.Remove(obj);
				pools[poolIndex].spawnedFromPoolButNotInPool.Add(obj);
				return obj;
			}
			else
			{
				if (pools[poolIndex].willGrow)
				{
					GameObject obj = createObject(poolIndex, true);
					obj = pools[poolIndex].poolStack.Pop();
					
					if (pools[poolIndex].willReturnEldestIfNoneAvailable)
					{
						pools[poolIndex].spawnedFromPoolButNotInPool.Add(obj);
					}
					
					return obj;
				}
				else
				{
					Debug.LogWarning("Error: Object pool empty, you may want to increase the pool size, " +
						"decrease the rate at which you are spawning from the pool, change returnToPoolTimer " +
						"or activate willReturnEldestIfNoneAvailable or WillGrow");
					return null;
				}
			}
		}
		else
		{
			GameObject obj = pools[poolIndex].poolStack.Pop();
			obj.SetActive(true);

			if (pools[poolIndex].willReturnEldestIfNoneAvailable)
			{
				pools[poolIndex].spawnedFromPoolButNotInPool.Add(obj);
			}

			return obj;
		}
	}

	public int findPoolIndexByName(string name)
	{
		for (int p = 0; p < pools.Length; p++)
		{
			if (pools[p].name == name)
			{
				return p;
			}
		}
		Debug.LogWarning("Error: Unable to find a pool named "+name);
		return -1;
	}

	public GameObject getPoolObjectByPoolName (string name)
	{
		int poolIndex = findPoolIndexByName (name);
		if (poolIndex >= 0 && poolIndex < pools.Length)
		{
			GameObject obj = GetPooledObject(poolIndex);
			return obj;
		}
		return null;
	}

	public void addToPoolByName (GameObject obj ,string name)
	{
		int poolIndex = findPoolIndexByName (name);
		if (poolIndex >= 0 && poolIndex < pools.Length)
		{
			AddToPool(obj, poolIndex);
		}
	}
}



[System.Serializable]
public class ObjectPool
{
	public string name;
	public GameObject prefab;
	public GameObject ParentOfPrefab;

	[HideInInspector]
	public Stack<GameObject> poolStack = new Stack<GameObject>();

	public int pooledAmount = 20; //how many objects to create.
	public float returnToPoolTimer = 0; //if set to 0 then objects don't return to the pool of their own accord,
	//otherwise they get deactivated and returned to the pool after returnToPoolTimer seconds 

	public bool willReturnEldestIfNoneAvailable = false;
	[HideInInspector]
	public List<GameObject> spawnedFromPoolButNotInPool = new List<GameObject>(); //list of objects that where created by the pooler 
	// but arent currently in the pool, used to return the eldest one if the bool above is set to true.
	public bool willGrow = false; //will the pool grow if it's empty and is asked to return an object???
	
	[HideInInspector]
	public int AmountInstantiated = 0;
}
