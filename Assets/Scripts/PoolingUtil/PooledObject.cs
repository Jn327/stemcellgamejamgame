﻿using UnityEngine;
using System.Collections;

public class PooledObject : MonoBehaviour {

	int poolIndex = 0;
	float deactivateTimer = 0;

	void OnEnable ()
	{
		if (deactivateTimer > 0) {
			StartCoroutine(returnToPoolInSeconds(deactivateTimer));
		}
	}

	public void SetPoolIndex (int i, float deactivate_Timer)
	{
		poolIndex = i;
		deactivateTimer = deactivate_Timer;
	}

	public IEnumerator returnToPoolInSeconds(float seconds)
	{
		yield return new WaitForSeconds (seconds);
		ObjectPooler.OP.AddToPool (gameObject, poolIndex);
		yield return null;
	}
}
