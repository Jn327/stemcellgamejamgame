﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Im imagining this will spawn the floor tiles and 

public class GameController : MonoBehaviour
{

    public GameObject tilePrefab;
    public Vector2 mapSize;
    public static GameObject[,] tilesMap; //2d array of tiles for easy acces and what not...
    public Transform tileSpawnCenter;
    public GameObject cell;

    public Color evenTileColor = Color.white;
    public Color oddTileColor = Color.black;

    public int numberOfPlayers = 2;

    // Use this for initialization
    void Start()
    {
		PlaceTilePrefabs(); //Places the tiles into the game.
        GameObject peicetest = CellCreater.Create_Cell(2, cell_type.type_b, new Vector2(5, 5), cell, avaliable_action.Attack);

        PlayerMenusController.instance.SetUpStemCellMenus(numberOfPlayers);
    }

    public void PlaceTilePrefabs()
    {
        tilesMap = new GameObject[(int)mapSize.x, (int)mapSize.y];
		List <TileController> create_tile_ref = new List<TileController> ();
        for (int x = 0; x < mapSize.x; x++)
        {
            for (int y = 0; y < mapSize.y; y++)
            {
                Vector3 tilePos = new Vector3(x, y, 0) - new Vector3(mapSize.x / 2, mapSize.y / 2, 0);

                if (tileSpawnCenter)
                    tilePos += tileSpawnCenter.position;

                GameObject tile = Instantiate(tilePrefab, tilePos, Quaternion.identity) as GameObject;
				tile.AddComponent <TileController> ();
				tile.GetComponent <TileController> ().Set_tile_position (new Vector2(x, y));
				create_tile_ref.Add (tile.GetComponent <TileController> ());
                tilesMap[x, y] = tile;
                tile.transform.parent = transform;

                tile.GetComponent<Renderer>().material.color = ((x % 2 == 0 && y % 2 == 0) || (x % 2 != 0 && y % 2 != 0)) ? evenTileColor : oddTileColor;
            }
        }
		for (int i = 0; i < create_tile_ref.Count; i++) {
			create_tile_ref[i].find_tiles_next_to ();
		}
    }

    // Update is called once per frame
    void Update()
    {
        handleTouchInput();
    }


    void handleTouchInput()
    {
        for (var i = 0; i < Input.touchCount; ++i)
        {
            if (Input.GetTouch(i).phase == TouchPhase.Began)
            {
                castRayFromScreenPosition(Input.GetTouch(i).position);
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            castRayFromScreenPosition(Input.mousePosition);
        }
    }

    void castRayFromScreenPosition(Vector3 position)
    {
        Ray ray = Camera.main.ScreenPointToRay(position);
        RaycastHit hit;

        if (Physics.Raycast (ray, out hit, 100))
		if (hit.collider.tag == "Cell")
			PlayerMenusController.instance.openMenuForPlayer (hit.collider.gameObject.GetComponent<CellStats> ());
    }
}
