﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerMenusController : MonoBehaviour
{
    public GameObject stemCellClickMenuPrefab;
    public GameObject attackCellMenuPrefab;
    
    public List<StemCellPlayerMenu> playerMenus;

    public static PlayerMenusController instance = null;


    void Awake()
    {
        if (instance != null)
        {
            GameObject.Destroy(instance.gameObject);
        }
        instance = this;
    }

    public void SetUpStemCellMenus(int numberPlayers)
    {
        playerMenus.Clear();

        GameObject stemMenu;
        GameObject attackMenu;

        for (int i = 0; i < numberPlayers; i++)
        {
            stemMenu = Instantiate(stemCellClickMenuPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            stemMenu.transform.SetParent(this.transform);
            CellMenuController stemMenuController = stemMenu.GetComponent<CellMenuController>();

            attackMenu = Instantiate(attackCellMenuPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            attackMenu.transform.SetParent(this.transform);
            CellMenuController attackMenuController = attackMenu.GetComponent<CellMenuController>();

            playerMenus.Add(new StemCellPlayerMenu(stemMenuController, attackMenuController, new Vector2(-1000, -1000)));
        }
    }
    public void openMenuForPlayer(CellStats cell)
    {
        int playerIndex = cell.Get_player_number() - 1;

        playerMenus[playerIndex].stemCellClickMenu.setcurrentCell(cell);
    }
}

[System.Serializable]
public class StemCellPlayerMenu
{
    public CellMenuController stemCellClickMenu;
    public CellMenuController attackCellMenu;

    public StemCellPlayerMenu (CellMenuController stemMenu, CellMenuController attackMenu, Vector2 pos)
    {
        stemCellClickMenu = stemMenu;
        attackCellMenu = attackMenu;

        stemMenu.setAnchoredPos(pos);
        attackMenu.setAnchoredPos(pos);
    }
}