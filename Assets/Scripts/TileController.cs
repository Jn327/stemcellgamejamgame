﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// A script that controlls the tile information, maybe holds stuff like nearby tiles, occupying cells....

public class TileController : MonoBehaviour 
{
	private List <GameObject> cells_on_tile = new List<GameObject> ();
	public List <GameObject> tile_next_to = new List<GameObject> ();

	public Vector2 tile_position;

	public Vector2 Get_tile_position () {
		return tile_position;
	}

	public void Set_tile_position (Vector2 position) {
		tile_position = position;
	}

	public void find_tiles_next_to ()
	{
		/*
		 * List tile nextto will have index numbers as follows
		 * North = 0
		 * East = 1
		 * South = 2
		 * West = 3
		 */

		if (tile_position.y + 1 < Camera.main.GetComponent<GameController> ().mapSize.y)
			tile_next_to.Add (GameController.tilesMap [(int)tile_position.x, (int)tile_position.y + 1]);
		if (tile_position.x + 1 < Camera.main.GetComponent<GameController> ().mapSize.x)
			tile_next_to.Add (GameController.tilesMap [(int)tile_position.x + 1, (int)tile_position.y]);
		if (tile_position.y - 1 >= 0)
			tile_next_to.Add (GameController.tilesMap [(int)tile_position.x, (int)tile_position.y - 1]);
		if (tile_position.x - 1 >= 0)
			tile_next_to.Add (GameController.tilesMap [(int)tile_position.x - 1, (int)tile_position.y]);
	}
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
