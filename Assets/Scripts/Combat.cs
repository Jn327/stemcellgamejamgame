﻿using UnityEngine;
using System.Collections;

public class Combat : MonoBehaviour 
{
	public enum Condition
	{
		win, loss, draw, error
	}

	public Condition determineOutcome(GameController attacker, GameController victim)
	{
		CellStats aStats = attacker.GetComponent<CellStats> ();
		CellStats vStats = victim.GetComponent<CellStats> ();
		if (vStats.Get_celltype() == aStats.Get_wintype())
        {
            return Condition.win;
        } 
        else if (vStats.Get_celltype() == aStats.Get_losetype())
        {
            return Condition.loss;
        }
        else
        {
            return Condition.draw;
        }
	}
}
