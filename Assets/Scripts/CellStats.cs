﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CellStats : MonoBehaviour 
{
	private avaliable_action action;
	private int player;
	private cell_type celltype;
	private cell_type wintype;
	private cell_type losetype;
	private Vector2 position;
	
	public int Get_player_number() {
		return player;
	}
	public cell_type Get_celltype () {
		return celltype;
	}
	public avaliable_action Get_action () {
		return action;
	}
	public Vector2 Get_position () {
		return position;
	}
	public cell_type Get_wintype(){
		return wintype;
	}
	public cell_type Get_losetype(){
		return losetype;
	}
	
	public void Set_player_number(int num) {
		player = num;
	}
	public void Set_celltype (cell_type type) {
		celltype = type;
		switch (type) 
		{
		case cell_type.attacker_type_a:
			Set_wintype(cell_type.attacker_type_b);
			Set_losetype(cell_type.attacker_type_c);
			break;
		case cell_type.attacker_type_b:
			Set_wintype(cell_type.attacker_type_c);
			Set_losetype(cell_type.attacker_type_a);
			break;
		case cell_type.attacker_type_c:
			Set_wintype(cell_type.attacker_type_a);
			Set_wintype(cell_type.attacker_type_b);
			break;
		}
	}
	public void Set_action (avaliable_action an_action) {
		action = an_action;
	}
	public void Set_position (Vector2 position) {
		gameObject.transform.position = GameController.tilesMap[(int)position.x, (int)position.y].transform.position;
	}
	public void Set_wintype(cell_type type){
		wintype = type;
	}
	public void Set_losetype(cell_type type){
		losetype = type;
	}

	private void check_adject_squares()
	{

	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
